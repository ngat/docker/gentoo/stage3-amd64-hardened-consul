#!/usr/bin/env sh

echo " -> Create temporary container"
docker create -it --cap-add ALL registry.gitlab.com/ngat/docker/gentoo/stage3-amd64-hardened-go /bin/bash

echo " -> Getting Temporary Container"
TMP_CONTAINER=`docker ps -a -l -q`

echo " -> Starting temporary container"
docker start $TMP_CONTAINER

echo " -> Creating package.use and package.accept_keywords directories"
docker exec -i $TMP_CONTAINER mkdir -p /etc/portage/package.accept_keywords
docker exec -i $TMP_CONTAINER mkdir -p /etc/portage/package.use

echo " -> Copying package.use files to /etc/portage/package.use/"
docker cp package.use/* $TMP_CONTAINER:/etc/portage/package.use/

echo " -> Copying package.accept_keywords files to /etc/portage"
docker cp package.accept_keywords/* $TMP_CONTAINER:/etc/portage/package.accept_keywords/

echo " -> Copying build.sh file to /build.sh"
docker cp build.sh $TMP_CONTAINER:/build.sh

echo " -> Running build.sh script"
docker exec -i $TMP_CONTAINER /bin/bash /build.sh

echo " -> Stopping temporary container"
docker stop $TMP_CONTAINER

DATE=$(date +%Y%m%d)
DOCKER_TAG="registry.gitlab.com/ngat/docker/gentoo/stage3-amd64-hardened-consul"
#echo " -> Squashing Container"
#docker export $TMP_CONTAINER | docker import -c "CMD /sbin/init" -
echo " -> Committing Container to Image"
docker commit -c "CMD /sbin/init" $TMP_CONTAINER $DOCKER_TAG:latest
docker rm $TMP_CONTAINER

echo " -> Attaching Tags and pushing to registry.gitlab.com"
NEW_IMAGE=`docker images -q | head -n1`

echo " -> Adding $DOCKER_TAG to $NEW_IMAGE"
docker tag $NEW_IMAGE $DOCKER_TAG:$DATE
